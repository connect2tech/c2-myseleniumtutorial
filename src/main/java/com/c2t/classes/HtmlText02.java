package com.c2t.classes;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class HtmlText02 {

	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();
		String baseUrl = "file:///D:/nchaurasia/solution-architect/Selenium2.0/SeleniumTutorial/src/main/resources/com/c2t/classes/HtmlText_02.html";
		// String tagName = "";
		driver.get(baseUrl);

		// locates elements by the value of their "id" attribute
		WebElement we = driver.findElement(By.id("lname"));
		String tag = we.getTagName();
		System.out.println(tag);
		/*WebElement wl = driver.findElement(By.id("lname"));
		String s = wl.getTagName();
		System.out.println(s);*/
		
		String val1 = driver.findElement(By.id("lname")).getAttribute("value");
		System.out.println(val1);
		
		System.out.println(driver.findElement(By.id("lname")).getText());
		
		

		/*
		 * driver.close(); System.exit(0);
		 */
	}
}
