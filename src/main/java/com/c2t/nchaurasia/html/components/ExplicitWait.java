package com.c2t.nchaurasia.html.components;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ExplicitWait {

	public static void main(String[] args) {

		WebDriver driver = new FirefoxDriver();
		long lStartTime = 0;
		long lEndTime = 0;
		long difference = 0;

		try {

			lStartTime = new Date().getTime();
			String baseUrl = "file:///D:/nchaurasia/solution-architect/Selenium2.0/SeleniumTutorial/src/main/resources/com/c2t/nchaurasia/html/components/MyFirstHtml.html";
			driver.get(baseUrl);

			WebDriverWait wait = new WebDriverWait(driver, 5);
			wait.until(ExpectedConditions.elementToBeClickable(driver
					.findElement(By.name("theButton"))));
			
			driver.findElement(By.name("theButton")).click();
			driver.switchTo().alert().accept();

			lEndTime = new Date().getTime();
			difference = lEndTime - lStartTime;

			System.out.println(difference);

		} catch (Exception e) {
			e.printStackTrace();
		}

		driver.close();
		System.exit(0);
	}
}
