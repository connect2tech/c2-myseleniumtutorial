package com.c2t.nchaurasia.html.components;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SwitchingTabs {
	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();
		
		driver.get("https://docs.oracle.com/javase/7/docs/api/");
		driver.switchTo().frame("classFrame");
        driver.findElement(By.linkText("Deprecated")).click();
        
        driver.findElement(By.linkText("Index")).click();
        
        driver.navigate().back();
        driver.navigate().back();
		
		//driver.quit();
	}

}
