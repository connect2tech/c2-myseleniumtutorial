package com.c2t.nchaurasia.html.components;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ImplicitWait {

	public static void main(String[] args) {

		WebDriver driver = new FirefoxDriver();
		long lStartTime = 0;
		long lEndTime = 0;
		long difference = 0;
		
		long lStartTime2 = 0;
		long lEndTime2 = 0;
		long difference2 = 0;

		try {
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
			String baseUrl = "file:///D:/nchaurasia/solution-architect/Selenium2.0/SeleniumTutorial/src/main/resources/com/c2t/nchaurasia/html/components/MyFirstHtml.html";
			// String tagName = "";
			driver.get(baseUrl);

			lStartTime = new Date().getTime();
			// finds elements based on the value of the "class" attribute
			String className = driver.findElement(By.className("w3-input"))
					.getTagName();
			System.out.println(className);
			lEndTime = new Date().getTime();
			difference = lEndTime - lStartTime;

			System.out.println("Elapsed milliseconds-1: " + difference);

			lStartTime2 = new Date().getTime();
			// finds elements based on the value of the "class" attribute
			className = driver.findElement(By.className("w-input"))
					.getTagName();
			System.out.println(className);

		} catch (Exception e) {
			lEndTime2 = new Date().getTime();
			difference2 = lEndTime2 - lStartTime2;
			System.out.println("Elapsed milliseconds-2: " + difference2);
			System.out.println("Elapsed seconds-2: " + (float)difference2/1000);
		}

		driver.close();
		System.exit(0);
	}
}
