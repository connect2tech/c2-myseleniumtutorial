package com.selenium.project1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class PageNavigation21_10_2016 {

	public static void main(String[] args) {

		WebDriver driver = new FirefoxDriver();
		String baseUrl = "http://localhost:8090/SpringMVC/user.htm";
		driver.get(baseUrl);
		
		WebElement we1 = driver.findElement(By.id("userName"));
		we1.sendKeys("tutorial");
		
		driver.findElement(By.name("_target1")).click();
		
		WebElement we2 = driver.findElement(By.id("password"));
		we2.sendKeys("password");
		
		driver.findElement(By.name("_target2")).click();
		
		WebElement we3 = driver.findElement(By.id("remark"));
		we3.sendKeys("remark");
		
		driver.findElement(By.name("_finish")).click();
		
		System.out.println(driver.getPageSource());
	
	}

}
