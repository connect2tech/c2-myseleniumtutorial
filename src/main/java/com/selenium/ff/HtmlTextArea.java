package com.selenium.ff;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class HtmlTextArea {

	public static void main(String[] args) {
		// declaration and instantiation of objects/variables
		WebDriver driver = new FirefoxDriver();
		String baseUrl = "file:///D:/nchaurasia/solution-architect/Selenium2.0/SeleniumTutorial/src/main/resources/com/c2t/nchaurasia/html/components/HtmlFormTextArea.html";
		String expectedTitle = "Page Title";
		String actualTitle = "";

		// launch Firefox and direct it to the Base URL
		driver.get(baseUrl);

		// get the actual value of the title
		actualTitle = driver.getTitle();

		

		// close Firefox
		driver.close();

		// exit the program explicitly
		System.exit(0);
	}

}