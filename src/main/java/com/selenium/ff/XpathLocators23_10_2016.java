package com.selenium.ff;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class XpathLocators23_10_2016 {

	public static void main(String[] args) {

		WebDriver driver = new FirefoxDriver();
		String baseUrl = "file:///D:/nchaurasia/solution-architect/Selenium2.0/SeleniumTutorial/src/main/resources/com/selenium/ff/XpathLocator23_10_2016.html";
		driver.get(baseUrl);

		String buttonText = driver.findElement(
				By.xpath("/html/body/form/fieldset/div/button")).getText();
		System.out.println(buttonText);

	}

}
