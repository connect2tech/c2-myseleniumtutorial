package com.selenium.ff;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class QuitVsClose {
	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();

		driver.get("http://www.popuptest.com/popuptest2.html");
		driver.close(); // using QUIT all windows will close

	}

}
