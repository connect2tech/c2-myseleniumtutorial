package com.selenium.ff;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class LocatingElements15_10_2016 {

	public static void main(String[] args) {

		WebDriver driver = new FirefoxDriver();
		String baseUrl = "file:///D:/nchaurasia/solution-architect/Selenium2.0/SeleniumTutorial/src/main/resources/com/selenium/ff/Selenium15_10_2016.html";
		driver.get(baseUrl);

/*		// finds elements based on the value of the "class" attribute
		String byClass = driver.findElement(By.className("w3-input"))
				.getTagName();
		System.out.println(byClass);

		// finds elements based on the driver's underlying CSS Selector engine
		String cssSelector = driver.findElement(By.cssSelector("input#fname"))
				.getTagName();
		System.out.println(cssSelector);*/

		
		
		// finds a link element by the exact text it displays
		String linkText = driver.findElement(By.className("w3-input"))
				.getText();
		System.out.println(linkText);
		
		
		

	/*  String name = driver.findElement(By.name("firstname")).getTagName();
		System.out.println(name);

		// locates elements that contain the given link text
		String partialLinkText = driver
				.findElement(By.partialLinkText("Click")).getTagName();
		System.out.println(partialLinkText);

		// locates elements by their tag name
		String tagName = driver.findElement(By.tagName("div")).getTagName();
		System.out.println(tagName);

		String getPageSource = driver.getPageSource();
		System.out.println(getPageSource);

		String getCurrentUrl = driver.getCurrentUrl();
		System.out.println(getCurrentUrl);

		// Fetches the inner text of the element that you specify
		String innerText = driver.findElement(By.name("theButton")).getText();
		System.out.println(innerText);

		driver.findElement(By.name("theButton")).click();
*/
		
	}

}
