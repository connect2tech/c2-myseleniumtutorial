package com.selenium.ff;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class HtmlButton {

	public static void main(String[] args) {
		// declaration and instantiation of objects/variables
		WebDriver driver = new FirefoxDriver();
		String baseUrl = "file:///D:/nchaurasia/solution-architect/Selenium2.0/SeleniumTutorial/src/main/resources/com/c2t/nchaurasia/html/components/HtmlButton.html";

		// launch Firefox and direct it to the Base URL
		driver.get(baseUrl);

		String innerText = driver.findElement(By.name("theButton")).getText();
		System.out.println(innerText);

		// close Firefox
		// driver.close();

		// exit the program explicitly
		// System.exit(0);
	}

}