package com.testng.java;

import org.testng.annotations.Test;

public class TestngGroupexample1 {

	@Test(groups = "db")
	public void myInfosysExample() {
		System.out.println("Connected to Infosys Database");
	}

	@Test(groups = "db")
	public void myTCSExample() {
		System.out.println("Connected to TCS Database");
	}

	@Test(groups = "db-Ibm")
	public void myIbm2Example() {
		System.out.println("Connected to Ibm2 Database");
	}

	@Test(groups = { "db", "broken" })
	public void myEatonExample() {
		System.out.println("Connected to Eaton Database");
	}
}
