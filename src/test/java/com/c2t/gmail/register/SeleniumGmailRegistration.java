package com.c2t.gmail.register;

import java.sql.Driver;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SeleniumGmailRegistration {

	WebDriver driver = null;
	String baseUrl = null;

	@BeforeTest
	public void beforeTest() {
		driver = new FirefoxDriver();
		//driver.manage().window().maximize();
		baseUrl = "https://accounts.google.com/SignUp?service=mail&hl=en&continue=http%3A%2F%2Fmail.google.com%2Fmail%2F%3Fpc%3Den-ha-apac-in-bk-refresh13&utm_campaign=en&utm_source=en-ha-apac-in-bk-refresh13&utm_medium=ha";
		driver.get(baseUrl);
	}

	@Test(priority = 2)
	public void clickNextStep() {
		WebElement we = driver.findElement(By.id("submitbutton"));
		we.click();

		pause();
	}

	@Test(priority = 1)
	public void getErrorMessageForNameBeforeNextStep() {

		WebElement we = driver.findElement(By.id("errormsg_0_LastName"));
		String text = we.getText();
		System.out.println("text::" + text);

	}

	@Test(priority = 3)
	public void getErrorMessageForNameAfterNextStep() {

		WebElement we = driver.findElement(By.id("errormsg_0_LastName"));
		String text = we.getText();
		System.out.println("text::" + text);

	}
	
	@Test(priority = 4)
	public void dropdown3() {

		JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0, 250);");

		driver.findElement(By.xpath("//div[@title='Location']")).sendKeys(
				"United");
		driver.findElement(By.xpath("//div[@title='Location']")).click();

		Actions actions = new Actions(driver);

		for (int i = 0; i < 5; i++) {

			actions.sendKeys(Keys.DOWN).build().perform();
			//pause();
		}
		actions.sendKeys(Keys.ENTER).build().perform();
		
		
		WebElement  we = driver.findElement(By.id("CountryCode"));
		System.out.println(we.getText());
		//we.click();
		pause();
		/*we.click();
		pause();
		
		Actions actions2 = new Actions(driver);

		for (int i = 0; i < 5; i++) {

			actions2.sendKeys(Keys.DOWN).build().perform();
			pause();
		}
		actions2.sendKeys(Keys.ENTER).build().perform();*/
	}

	@Test(priority = 4)
	public void dropdown() {

		/*
		 * driver.findElement(By.id(":d")).click(); for(int i = 0; i <= 0; i++){
		 * Actions actions = new Actions(driver);
		 * actions.sendKeys(Keys.DOWN).build().perform();//press down arrow key
		 * Actions actions2 = new Actions(driver);
		 * actions2.sendKeys(Keys.ENTER).build().perform();//press enter
		 * //pause(); }
		 */

		/*JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0, 250);");*/

		/*
		 * driver.findElement(By.xpath("//div[@title='Location']")).sendKeys(
		 * "United");
		 */
		
		for (int j = 0; j < 10; j++) {
			List<WebElement>  we = driver.findElements(By.xpath("//div[@class='goog-inline-block goog-flat-menu-button-caption']"));
			we.get(2).click();

			Actions actions = new Actions(driver);
			for (int i = 0; i < 100; i++) {

				actions.sendKeys(Keys.DOWN).build().perform();
				//pause();
			}
			actions.sendKeys(Keys.ENTER).build().perform();
			
		}

		

		/*
		 * actions.sendKeys(Keys.DOWN).build().perform(); pause();
		 * actions2.sendKeys(Keys.ENTER).build().perform(); pause();
		 */

	}
	
	@Test(priority = 4)
	public void dropdown2() {

		/*JavascriptExecutor jse = (JavascriptExecutor) driver;
		jse.executeScript("scroll(0, 250);");

		List<WebElement> we = driver
				.findElements(By
						.xpath("//div[@class='goog-inline-block goog-flat-menu-button-caption']"));
		we.get(2).click();

		Actions actions = new Actions(driver);*/

		/*for (int j = 0; j < 15; j++) {
			for (int i = 0; i < 100; i++) {

				actions.sendKeys(Keys.DOWN).build().perform();
			}
			actions.sendKeys(Keys.ENTER).build().perform();
			pause();
		}*/
		
		/*for (int j = 0; j < 1; j++) {
			for (int i = 0; i < 50; i++) {

				actions.sendKeys(Keys.DOWN).build().perform();
			}
			actions.sendKeys(Keys.ENTER).build().perform();
			pause();
		}*/
		
		//driver.findElement(By.xpath("//div[@title='Location']")).click();
		//driver.findElement(By.xpath("//div[@role='option']/text()='August'")).click();
		
		
		
		System.out.println(driver.findElement(By.xpath("//div[@id=':w']")).getText());

	}

	@Test(priority = 4)
	public void dropdown22() {

		driver.findElement(By.xpath("//div[@title='Location']")).sendKeys(
				"United");
	}
	
	@AfterTest
	public void afterTest() {
		//driver.quit();
	}

	private void pause() {
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
