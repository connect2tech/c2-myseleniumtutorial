package com.c2t.mayuresh;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Timeouts;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;


public class GmailRegistrationXPath {

	WebDriver driver = new FirefoxDriver();
	String url;
	
	@BeforeTest
	public void openBrowser(){
		url = "https://accounts.google.com/SignUp?service=mail&continue=https://mail.google.com/mail/?pc=topnav-about-en";
		driver.get(url);
	}

	@Test
	public void login(){
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		//absolute xpath
		//WebElement firstName = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div[1]/div/form/div[1]/fieldset/label[1]/input"));
		//relative xpath
		WebElement firstName = driver.findElement(By.xpath("//body//form/div[1]//label[1]/input"));
		firstName.sendKeys("mary");
		//absolute xpath
		//WebElement lastName = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div[1]/div/form/div[1]/fieldset/label[2]/input"));
		//relative xpath
		WebElement lastName = driver.findElement(By.xpath("//body//form/div[1]//label[2]/input"));
		lastName.sendKeys("james");
		//absolute xpath
		//WebElement userName = driver.findElement(By.xpath("html/body/div[1]/div[2]/div/div[1]/div/form/div[2]/label/input"));
		//relative xpath
		WebElement userName = driver.findElement(By.xpath("//body//form/div[2]/label/input"));
		userName.sendKeys("mary.james");
		
	}
	
	@AfterTest
	public void closeBrowser(){
		//driver.close();
		//System.exit(0);
	}
}
