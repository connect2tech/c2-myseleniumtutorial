package com.c2t.mayuresh;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriver.Timeouts;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestAssignment14Jan {

	WebDriver driver = new FirefoxDriver();
	String url;
	
	@BeforeTest
	public void openBrowser(){
		url = "https://www.gmail.com";
		driver.get(url);
	}
	
	@Test
	public void login(){
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		if(driver.findElement(By.id("Email")).isDisplayed()==true){
			WebElement email = driver.findElement(By.id("Email"));
			email.sendKeys("mayur16");
			WebElement next = driver.findElement(By.id("next"));
			next.click();
			driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
			if(driver.findElement(By.id("Passwd")).isDisplayed()==true){
				WebElement pwd = driver.findElement(By.id("Passwd"));
				pwd.sendKeys("var16rav");
				WebElement persistentCookie = driver.findElement(By.id("PersistentCookie"));
				if(persistentCookie.isSelected()==true){
					persistentCookie.click();
				}
				WebElement signIn = driver.findElement(By.id("signIn"));
				signIn.click();
			}
			driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
			if(driver.findElement(By.className("gb_b gb_db gb_R")).isDisplayed()==true){
				WebElement account = driver.findElement(By.className("gb_b gb_db gb_R"));
				account.click();
			}
			
		}
	}
	
	@AfterTest
	public void closeBrowser(){
		//driver.close();
	}
}
