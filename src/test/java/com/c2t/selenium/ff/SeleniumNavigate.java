package com.c2t.selenium.ff;

import java.sql.Driver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SeleniumNavigate {

	WebDriver driver = null;
	String baseUrl = null;

	@BeforeTest
	public void beforeTest() {
		driver = new FirefoxDriver();
		baseUrl = "file:///D:/nchaurasia/solution-architect/Selenium2.0/SeleniumTutorial/src/main/resources/com/selenium/ff/Selenium15_10_2016.html";
	}

	@Test
	public void testNavigateTo() {
		driver.navigate().to(baseUrl);
	}

	@Test
	public void testNavigateRefresh() {
		driver.navigate().refresh();
	}

	@Test
	public void testNavigateBack() {
		driver.navigate().back();
	}

	@Test
	public void testNavigateForward() {
		driver.navigate().forward();
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}
}
