package com.c2t.selenium.ff;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SeleniumTestPriority23_10_2016 {

	@Test(priority=2)
	public void testA() {
		System.out.println("This is TestNG-Maven Example");
	}
	
	@Test(priority=1)
	public void testB() {
		System.out.println("This is TestNG-Maven Example");
	}
	
	@Test(priority=0)
	public void testC() {
		System.out.println("This is TestNG-Maven Example");
	}
	
	@Test(priority=-1)
	public void testD() {
		System.out.println("This is TestNG-Maven Example");
	}
	
	@Test(priority=-1)
	public void testE() {
		System.out.println("This is TestNG-Maven Example");
	}

}
