package com.c2t.selenium.ff;

import java.sql.Driver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SeleniumGetMethods {

	WebDriver driver = null;
	String baseUrl = null;

	@BeforeTest
	public void beforeTest() {
		driver = new FirefoxDriver();
		baseUrl = "file:///D:/nchaurasia/solution-architect/Selenium2.0/SeleniumTutorial/src/main/resources/com/selenium/ff/Selenium15_10_2016.html";
		driver.get(baseUrl);
	}

	@Test
	public void testGetTitle() {
		String getTitle = driver.getTitle();
		System.out.println("getTitle------->"+getTitle);
	}
	
	@Test
	public void testGetPageSource() {
		String getPageSource = driver.getPageSource();
		System.out.println("getPageSource------->"+getPageSource);
	}
	
	@Test
	public void testGetCurrentUrl() {
		String getCurrentUrl = driver.getCurrentUrl();
		System.out.println("getCurrentUrl------->"+getCurrentUrl);
	}
	
	@Test
	public void testGetText() {
		String getText = driver
				.findElement(By.partialLinkText("Click")).getText();
		System.out.println("getText------->"+getText);
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}
}
