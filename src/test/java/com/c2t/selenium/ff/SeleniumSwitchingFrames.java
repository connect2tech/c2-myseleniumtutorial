package com.c2t.selenium.ff;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SeleniumSwitchingFrames {

	WebDriver driver = null;
	String baseUrl = null;

	@BeforeTest
	public void beforeTest() {
		driver = new FirefoxDriver();
		baseUrl = "http://seleniumhq.github.io/selenium/docs/api/java/";
		driver.get(baseUrl);
	}

	@Test
	public void testSwitchingFrames() {
		driver.switchTo().frame("packageListFrame");
        WebElement we = driver.findElement(By.className("indexHeader"));
        we.findElement(By.linkText("All Classes")).click();
	}
	
	@Test
	public void testSwitchingAlert() {
		driver.switchTo().frame("packageListFrame");
        WebElement we = driver.findElement(By.className("indexHeader"));
        we.findElement(By.linkText("All Classes")).click();
	}

		@AfterTest
	public void afterTest() {
		driver.quit();
	}
}
