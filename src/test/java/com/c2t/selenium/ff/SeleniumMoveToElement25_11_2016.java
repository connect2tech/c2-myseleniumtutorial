package com.c2t.selenium.ff;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class SeleniumMoveToElement25_11_2016 {

	WebDriver driver = null;
	String baseUrl = null;

	@BeforeTest
	public void beforeTest() {
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		baseUrl = "http://c2t.nchaurasia.in/";
		driver.get(baseUrl);
	}

	@Test(priority = 1, enabled = false)
	public void testMoveToElementA() {
		WebElement courses = driver.findElement(By.linkText("Courses"));
		Actions action = new Actions(driver);
		action.moveToElement(courses).perform();
		//action.moveToElement(courses).build().perform();
		driver.findElement(By.linkText("Selenium2")).click();
	}

	@Test(priority = 2, enabled = false)
	public void testMoveToElementB() {
		WebElement course = driver.findElement(By.linkText("Courses"));
		Actions action = new Actions(driver);
		action.moveToElement(course).perform();
		WebElement selenium2 = driver.findElement(By.linkText("Selenium2"));
		action.moveToElement(selenium2);
		action.click();
		action.perform();
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}
}
