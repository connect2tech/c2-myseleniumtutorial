package com.c2t.selenium.ff;

import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SeleniumCrossBrowserTesting {

	WebDriver driver = null;
	String baseUrl = null;

	@BeforeTest
	public void beforeTest() {
		driver = new FirefoxDriver();
		baseUrl = "file:///D:/nchaurasia/solution-architect/Selenium2.0/SeleniumTutorial/src/main/resources/com/selenium/ff/Selenium15_10_2016.html";
		driver.get(baseUrl);
	}

	@Test
	public void testByClassName() {
		String className = driver.findElement(By.className("w3-input"))
				.getTagName();
		System.out.println("className------------>" + className);
	}

	@Test
	public void testByCssSelector() {
		String cssSelector = driver.findElement(By.cssSelector("input#fname"))
				.getTagName();
		System.out.println("cssSelector------------>" + cssSelector);
	}

	@Test
	public void testById() {
		String id = driver.findElement(By.id("fname")).getTagName();
		System.out.println("id------------>" + id);
	}

	@Test
	public void testByLinkText() {
		String linkText = driver.findElement(By.linkText("Click Me"))
				.getTagName();
		System.out.println("linkText------------>" + linkText);

	}

	@Test
	public void testByName() {
		String name = driver.findElement(By.name("firstname")).getTagName();
		System.out.println("name------------>" + name);
	}

	@Test
	public void testByPartialLinkText() {
		String partialLinkText = driver
				.findElement(By.partialLinkText("Click")).getTagName();
		System.out.println("partialLinkText------------>" + partialLinkText);
	}

	@Test
	public void testByTagName() {
		String tagName = driver.findElement(By.tagName("div")).getTagName();
		System.out.println("tagName------------>" + tagName);
	}

	@Test
	public void testByTagNameSpan() {
		List<WebElement> divs= driver.findElements(By.tagName("div"));
		
		WebElement we = divs.get(1).findElement(By.tagName("p"));
		System.out.println("we---->"+we.getText());
		List<WebElement> spans = we.findElements(By.tagName("span"));
		for(WebElement span:spans){
			System.out.println(span.getText());
		}
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}
}

