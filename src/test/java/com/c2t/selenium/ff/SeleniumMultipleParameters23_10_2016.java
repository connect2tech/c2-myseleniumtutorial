package com.c2t.selenium.ff;

import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SeleniumMultipleParameters23_10_2016 {

	@Test(priority=1)
	public void testA() {
		System.out.println("This is TestNG-Maven Example");
	}
	
	@Test(priority=2,enabled=false)
	public void testB() {
		System.out.println("This is TestNG-Maven Example");
	}
	
	
}
