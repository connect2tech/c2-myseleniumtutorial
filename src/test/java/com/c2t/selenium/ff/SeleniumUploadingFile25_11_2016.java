package com.c2t.selenium.ff;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class SeleniumUploadingFile25_11_2016 {

	@BeforeTest
	public void beforeTest() {
	}

	@Test(priority = 1, enabled = true)
	public void testMoveToElementA() {
		String baseUrl = "http://www.megafileupload.com/";
		WebDriver driver = new FirefoxDriver();

		driver.get(baseUrl);
		WebElement uploadElement = driver.findElement(By.id("uploadfile_0"));
		// enter the file path onto the file-selection input field
		uploadElement.sendKeys("D:\\test1.txt");
		// check the "I accept the terms of service" check box
		driver.findElement(By.id("terms")).click();
		// click the "UploadFile" button
		driver.findElement(By.name("send")).click();
	}

	@AfterTest
	public void afterTest() {
	}
}
