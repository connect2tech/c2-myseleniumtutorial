package com.c2t.session4;

import java.sql.Driver;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SeleniumXPath {

	WebDriver driver = null;
	String baseUrl = null;

	@BeforeTest
	public void beforeTest() {
		driver = new FirefoxDriver();
		baseUrl = "http://store.demoqa.com/http://store.demoqa.com/";
		driver.get(baseUrl);
	}

	@Test(priority = 1)
	public void findElementAbsoluteXPath() {
		String text = driver
				.findElement(
						By.xpath("/html/body/div[2]/div/div/footer/section[3]/div/ul/li[3]/a"))
				.getText();
		Assert.assertEquals(text, "RSS");
	}

	@Test
	public void findElementRelativeXPath1() {
		String text = driver.findElement(
				By.xpath(".//*[@id='social-media']/ul/li[3]/a")).getText();
		Assert.assertEquals(text, "RSS");
	}
	
	@Test
	public void findElementRelativeXPath2() {
		String text = driver.findElement(
				By.xpath("//body//footer/section[3]//div/ul/li[3]/a")).getText();
		System.out.println("text------->"+text);
		Assert.assertEquals(text, "RSS");
	}
	
	@Test
	public void findElementXPathContains() {
		driver.findElement(
				By.xpath("//img[contains(@src,'timthumb')]")).click();
	}
	
	@Test
	public void findElementXPathStartsWith() {
		pause();
		driver.findElement(
				By.xpath("//a[starts-with(@title,'Get Fed on our Feeds')]")).click();
	}
	
	@Test
	public void findElementXPathText() {
		pause();
		driver.findElement(By.xpath(".//*[text()='Latest Blog Post:']"));
	}
	

	@AfterTest
	public void afterTest() {
		//driver.quit();
	}

	private void pause() {
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
