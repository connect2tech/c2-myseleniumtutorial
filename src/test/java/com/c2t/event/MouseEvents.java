package com.c2t.event;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class MouseEvents {

	@BeforeTest
	public void beforeTest() {
	}

	@Test(priority = 1, enabled = true)
	public void testMoveToElement() {
		String baseUrl = "http://newtours.demoaut.com/";
		WebDriver driver = new FirefoxDriver();

		driver.get(baseUrl);
		WebElement link_Home = driver.findElement(By.linkText("Home"));
		
		
		WebElement td_Home = driver.findElement(By.xpath("//html/body/div"
				+ "/table/tbody/tr/td" + "/table/tbody/tr/td"
				+ "/table/tbody/tr/td" + "/table/tbody/tr"));

		Actions builder = new Actions(driver);
		Action mouseOverHome = builder.moveToElement(link_Home).build();

		String bgColor = td_Home.getCssValue("background-color");
		System.out.println("Before hover: " + bgColor);
		
		mouseOverHome.perform();
		
		bgColor = td_Home.getCssValue("background-color");
		System.out.println("After hover: " + bgColor);
		driver.quit();
	}

	@Test(priority = 1, enabled = true)
	public void testBuildingSeriesOfMultipleActions() {
		String baseUrl = "http://www.facebook.com";
		WebDriver driver = new FirefoxDriver();

		driver.get(baseUrl);
		WebElement txtUsername = driver.findElement(By.id("email"));

		Actions builder = new Actions(driver);
		Action seriesOfAction = builder.moveToElement(txtUsername).click()
				.keyDown(txtUsername, Keys.SHIFT).sendKeys(txtUsername,"hello")
				.doubleClick().build();
		
		//seriesOfAction.perform();
		//driver.quit();

	}

	@AfterTest
	public void afterTest() {

	}
}
