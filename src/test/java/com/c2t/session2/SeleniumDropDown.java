package com.c2t.session2;

import java.sql.Driver;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SeleniumDropDown {

	WebDriver driver = null;
	String baseUrl = null;
	List l;

	@BeforeTest
	public void beforeTest() {
		driver = new FirefoxDriver();
		baseUrl = "file:///D:/nchaurasia/solution-architect/Selenium2.0/SeleniumTutorial/src/main/resources/com/c2t/session2/SeleniumInputRadioCheckboxDropdownEtc.html";
		driver.get(baseUrl);
	}

	@BeforeMethod
	public void beforeMethod() {
		l = new ArrayList();
	}

	@Test(priority = 1)
	public void classNameTest() {
		WebElement we = driver.findElement(By.className("w3-input"));
		String tagName = we.getTagName();
		String text = we.getText();
		l.add(tagName);
		l.add(text);
		System.out.println("l=" + l);

	}

	@Test(priority = 2)
	public void cssSelectorTest1() {
		WebElement we = driver.findElement(By.cssSelector("input#fname"));
		// WebElement we = driver.findElement(By.cssSelector(".w3-input"));
		String tagName = we.getTagName();
		String text = we.getText();
		l.add(tagName);
		l.add(text);
		System.out.println("l=" + l);

	}

	@Test(priority = 3)
	public void cssSelectorTest2() {
		WebElement we = driver.findElement(By.cssSelector(".w3-input"));
		String tagName = we.getTagName();
		String text = we.getText();
		l.add(tagName);
		l.add(text);
		System.out.println("l=" + l);
	}

	@Test(priority = 4)
	public void idTest() {
		WebElement we = driver.findElement(By.id("fname"));
		String tagName = we.getTagName();
		String text = we.getText();
		l.add(tagName);
		l.add(text);
		System.out.println("l=" + l);
	}

	@Test(priority = 5)
	public void linkTextTest() {
		WebElement we = driver.findElement(By.linkText("Click Me"));
		String tagName = we.getTagName();
		String text = we.getText();
		l.add(tagName);
		l.add(text);
		System.out.println("l=" + l);
	}

	@Test(priority = 6)
	public void nameTest() {
		WebElement we = driver.findElement(By.name("firstname"));
		String tagName = we.getTagName();
		String text = we.getText();
		l.add(tagName);
		l.add(text);
		System.out.println("l=" + l);
	}

	@Test(priority = 7)
	public void partialLinkTextTest() {
		WebElement we = driver.findElement(By.partialLinkText("Click"));
		String tagName = we.getTagName();
		String text = we.getText();
		l.add(tagName);
		l.add(text);
		System.out.println("l=" + l);
	}

	@Test(priority = 8)
	public void tagNameTest() {
		WebElement we = driver.findElement(By.tagName("div"));
		String tagName = we.getTagName();
		String text = we.getText();
		l.add(tagName);
		l.add(text);
		System.out.println("l=" + l);
	}

	@AfterMethod
	public void afterMethod() {
		l = null;
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}
}
