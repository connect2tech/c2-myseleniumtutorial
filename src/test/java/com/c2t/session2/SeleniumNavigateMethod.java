package com.c2t.session2;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SeleniumNavigateMethod {

	WebDriver driver = null;
	final private String baseUrl = "file:///D:/nchaurasia/solution-architect/Selenium2.0/SeleniumTutorial/src/main/resources/com/c2t/session1/HtmlButtonClick.html";

	@BeforeTest
	public void beforeTest() {
		driver = new FirefoxDriver();
		driver.navigate().to(baseUrl);
	}

	@Test(priority = 1)
	public void refreshTest() {
		driver.navigate().refresh();
	}

	@Test(priority = 2)
	public void backTest() {
		driver.navigate().back();
	}

	@Test(priority = 3)
	public void forwardTest() {
		driver.navigate().forward();
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}
}
