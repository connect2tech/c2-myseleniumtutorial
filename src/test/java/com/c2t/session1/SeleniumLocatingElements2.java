package com.c2t.session1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SeleniumLocatingElements2 {

	public static void main(String[] args) {
		WebDriver driver = new FirefoxDriver();
		String baseUrl = "file:///D:/nchaurasia/solution-architect/Selenium2.0/SeleniumTutorial/src/main/resources/com/c2t/nchaurasia/html/components/MyFirstHtml.html";
		// String tagName = "";
		driver.get(baseUrl);

		// locates elements by the value of their "id" attribute
		// finds a link element by the exact text it displays
		

		// locates elements by the value of the "name" attribute

		// locates elements that contain the given link text
		String partialLinkText = driver
				.findElement(By.partialLinkText("Click")).getTagName();
		System.out.println(partialLinkText);

		// locates elements by their tag name
		String tagName = driver.findElement(By.tagName("div")).getTagName();
		System.out.println(tagName);

		// Setting value in textbox
		// driver.findElement(By.id("fname")).sendKeys("tutorial");

		// driver.findElement(By.name("theButton")).click();

		/*String title = driver.getTitle();
		System.out.println(title);*/

		String getPageSource = driver.getPageSource();
		System.out.println(getPageSource);

		String getCurrentUrl = driver.getCurrentUrl();
		System.out.println(getCurrentUrl);

		// Fetches the inner text of the element that you specify
		String innerText = driver.findElement(By.name("theButton")).getText();
		System.out.println(innerText);

		driver.close();
		System.exit(0);
	}
}
