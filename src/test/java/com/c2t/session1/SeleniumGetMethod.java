package com.c2t.session1;

import java.sql.Driver;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class SeleniumGetMethod {

	WebDriver driver = null;
	final private String baseUrl = "file:///D:/nchaurasia/solution-architect/Selenium2.0/SeleniumTutorial/src/main/resources/com/c2t/session1/HtmlButtonClick.html";

	@BeforeTest
	public void beforeTest() {
		driver = new FirefoxDriver();
		driver.get(baseUrl);
	}

	@Test(priority = 1)
	public void getTitleTest() {
		String expectedTitle = "My First Selenium!!!";
		String actualTitle = driver.getTitle();
		Assert.assertEquals(actualTitle, expectedTitle,
				"The title is not correct");
	}

	@Test(priority = 2)
	public void getPageSourceTest() {
		String pageSource = driver.getPageSource();
		System.out.println("pageSource-->" + pageSource);
	}

	@Test(priority = 3)
	public void getCurrentUrlTest() {
		String actualUrl = driver.getCurrentUrl();
		Assert.assertEquals(actualUrl, baseUrl, "The title is not correct");
	}

	@AfterTest
	public void afterTest() {
		driver.quit();
	}
}
