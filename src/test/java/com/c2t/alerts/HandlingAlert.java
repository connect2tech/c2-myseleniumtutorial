package com.c2t.alerts;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

public class HandlingAlert {

	WebDriver driver;

	@BeforeTest
	public void beforeTest() {
		driver = new FirefoxDriver();
	}

	@Test(priority = 1, enabled = true)
	public void alerts() {

		driver.get("http://demo.guru99.com/V4/");

		driver.findElement(By.name("uid")).sendKeys("mngr66135");
		driver.findElement(By.name("password")).sendKeys("vymehEq");
		driver.findElement(By.name("btnLogin")).submit();
		driver.findElement(By.linkText("Delete Customer")).click();
		driver.findElement(By.name("cusid")).sendKeys("53920");
		driver.findElement(By.name("AccSubmit")).click();

		pause();

		// Switching to Alert
		Alert alert = driver.switchTo().alert();

		// Capturing alert message.
		String alertMessage = driver.switchTo().alert().getText();

		// Displaying alert message
		System.out.println(alertMessage);

		pause();
		
		alert.accept();
		//alert.dismiss();

	}


	public void pause() {

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
