package com.c2t.reporting;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

@Listeners(ITestListenerImpl.class)
public class ITestListenerTc {
	
	WebDriver driver = new FirefoxDriver();
	
	@BeforeTest
	public void starting(){
		driver.get("http://google.com");
	}

	@Test
	public void testRealReportOne() {

		System.out.println("---testRealReportOne---");
		Assert.assertTrue(true);

	}

	@Test
	public void testRealReportTwo() {

		System.out.println("---testRealReportTwo---");
		Assert.assertTrue(false);

	}

	// Test case depends on failed testcase= testRealReportTwo

	@Test(dependsOnMethods = "testRealReportTwo")
	public void testRealReportThree() {

	}

}
