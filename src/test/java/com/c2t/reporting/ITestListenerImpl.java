package com.c2t.reporting;

import java.util.Random;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;

import org.testng.ITestListener;

import org.testng.ITestResult;

import com.c2t.reporting.screenshot.BaseClass;

public class ITestListenerImpl implements ITestListener {
	
	

	public void onStart(ITestContext arg0) {
		System.out.println("Start Of Execution(TEST)->" + arg0.getName());
		
	}

	public void onTestStart(ITestResult arg0) {
		System.out.println("Test Started->" + arg0.getName());
	}

	public void onTestSuccess(ITestResult arg0) {
		System.out.println("Test Pass->" + arg0.getName());
		String file = System.getProperty("user.dir") + "\\" + "screenshot"
				+ (new Random().nextInt()) + ".png";
		try {
			BaseClass.takeSnapShot(BaseClass.getDriver(), file);
			System.out.println(file);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void onTestFailure(ITestResult arg0) {
		System.out.println("Test Failed->" + arg0.getName());
	}

	public void onTestSkipped(ITestResult arg0) {
		System.out.println("Test Skipped->" + arg0.getName());
	}

	public void onFinish(ITestContext arg0) {
		System.out.println("END Of Execution(TEST)->" + arg0.getName());
	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult arg0) {
		// TODO Auto-generated method stub
	}

}